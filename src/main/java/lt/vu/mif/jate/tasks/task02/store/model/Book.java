package lt.vu.mif.jate.tasks.task02.store.model;

import lombok.Getter;
import lombok.Singular;
import lt.vu.mif.jate.tasks.task02.search.Searchable;
import lt.vu.mif.jate.tasks.task02.store.Store;
import lt.vu.mif.jate.tasks.task02.store.model.builderHelpers.AuthorHelper;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created on 3/9/16.
 *
 * @author invertisment
 */
public class Book extends Item {
    @Getter
    private final Integer pages;
    @Getter
    @Searchable(field = Store.PUBLISHER_FIELD)
    private final String publisher;

    @lombok.Builder
    public Book(BigInteger id, String title, String subtitle, @Singular Set<Category> categories, Set<Author> authors, String description, Rating ratings, Integer pages, String publisher) {
        super(id, title, subtitle, categories, authors, description, ratings);
        this.pages = pages;
        this.publisher = publisher;
    }

    public Book(BigInteger id, String title, String subtitle, Set<Category> categories, Set<Author> authors, String description, Collection<Integer> ratings, Integer pages, String publisher) {
        super(id, title, subtitle, categories, authors, description, new Rating(ratings));
        this.pages = pages;
        this.publisher = publisher;
    }

    public Book(BigInteger id, String title, String description) {
        super(id, title, description);
        this.pages = 0;
        this.publisher = ";sdfhsjkdfh";
    }

    public static class Builder {
        private final BookBuilder bookBuilder = builder();
        private final AuthorHelper authorHelper = new AuthorHelper();

        public Builder(BigInteger id, String title, String description) {
            super();
            bookBuilder.id(id);
            bookBuilder.title(title);
            bookBuilder.description(description);
        }

        public Builder author(String author) {
            authorHelper.author(author);
            return this;
        }

        public Builder rating(double rating, int count) {
            authorHelper.rating(rating, count);
            return this;
        }

        public Builder category(String category) {
            authorHelper.category(category);
            return this;
        }

        public Builder subtitle(String subtitle) {
            bookBuilder.subtitle(subtitle);
            return this;
        }

        public Builder publisher(String publisher) {
            bookBuilder.publisher(publisher);
            return this;
        }

        public Builder pages(int pages) {
            bookBuilder.pages(pages);
            return this;
        }

        public Book build() {
            bookBuilder.authors(authorHelper.getAuthors().stream().map(Author::new).collect(Collectors.toSet()));
            bookBuilder.ratings(authorHelper.getRatings());
            bookBuilder.categories(authorHelper.getCategories());
            return bookBuilder.build();
        }
    }
}
