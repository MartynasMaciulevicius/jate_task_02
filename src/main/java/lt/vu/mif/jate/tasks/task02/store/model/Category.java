package lt.vu.mif.jate.tasks.task02.store.model;

import lombok.AllArgsConstructor;

/**
 * Created by martin on 3/10/16.
 */
@AllArgsConstructor
public class Category {
    public final String name;

    @Override
    public String toString() {
        return name;
    }

}
