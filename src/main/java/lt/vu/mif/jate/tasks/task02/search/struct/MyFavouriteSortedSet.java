package lt.vu.mif.jate.tasks.task02.search.struct;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created on 5/7/16.
 *
 * @author invertisment
 */
public class MyFavouriteSortedSet<T extends Comparable<T>> implements SortedSet<T> {

    private final Set<T> set;

    private final Comparator<? super T> comparator = Comparable::compareTo;

    public MyFavouriteSortedSet(Set<T> set) {
        this.set = set.stream()
                .sorted(comparator)
                .collect(Collectors.toSet());
    }

    @Override
    public Comparator<? super T> comparator() {
        return comparator;
    }

    @Override
    public SortedSet<T> subSet(T fromElement, T toElement) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public SortedSet<T> headSet(T toElement) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public SortedSet<T> tailSet(T fromElement) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public T first() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public T last() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public int size() {
        return this.set.size();
    }

    @Override
    public boolean isEmpty() {
        return this.set.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return this.set.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return set.stream().sorted(comparator).iterator();
    }

    @Override
    public Object[] toArray() {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean add(T t) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean remove(Object o) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new RuntimeException("Unimplemented");
    }

    @Override
    public void clear() {
        throw new RuntimeException("Unimplemented");
    }
}
