package lt.vu.mif.jate.tasks.task02.store.model;

/**
 * Created by martin on 3/9/16.
 */
public class WrongRatingException extends RuntimeException {

    private final static String message = "Rating value must be between 1 and 5. Got %s";

    public WrongRatingException(int rating) {
        super(String.format(message, rating));
    }
}
