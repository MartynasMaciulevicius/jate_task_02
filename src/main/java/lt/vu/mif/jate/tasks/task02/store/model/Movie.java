package lt.vu.mif.jate.tasks.task02.store.model;

import lombok.Singular;
import lt.vu.mif.jate.tasks.task02.store.model.builderHelpers.RatingHelper;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Set;

/**
 * Created on 3/9/16.
 *
 * @author invertisment
 */
public class Movie extends Item {

    public Movie(BigInteger id, String title, String subtitle, Set<Category> categories, Set<Author> authors, String description, Collection<Integer> ratings) {
        this(id, title, subtitle, categories, authors, description, new Rating(ratings));
    }

    @lombok.Builder
    public Movie(BigInteger id, String title, String subtitle, @Singular Set<Category> categories, Set<Author> authors, String description, Rating ratings) {
        super(id, title, subtitle, categories, authors, description, ratings);
    }

    public Movie(BigInteger id, String title, String description) {
        super(id, title, description);
    }

    public static class Builder {
        private final Movie.MovieBuilder builder = builder();

        private final RatingHelper ratingHelper = new RatingHelper();

        public Builder(BigInteger id, String title, String description) {
            super();
            builder.id(id);
            builder.title(title);
            builder.description(description);
        }

        public Builder rating(double rating, int count) {
            ratingHelper.rating(rating, count);
            return this;
        }

        public Builder category(String category) {
            ratingHelper.category(category);
            return this;
        }

        public Movie build() {
            builder.ratings(ratingHelper.getRatings());
            ratingHelper.getCategories().stream().forEach(builder::category);
            return builder.build();
        }
    }

}
