package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.ObjectHelper;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created on 5/10/16.
 *
 * @author invertisment
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class SingularSearchResult<T> implements BaseSearchResult<T, SingularSearchResult<T>> {
    private final T result;
    private final Optional<Object> fieldValue;

    public static <T> Stream<SingularSearchResult<T>> create(T result, Optional<Object> fieldValue) {
        return ObjectHelper.unwrapParentOptional(
                ObjectHelper.convertMaybeCollection(fieldValue))
                .map(maybeObject -> new SingularSearchResult<>(result, maybeObject));
    }

    @Override
    public Stream<SingularSearchResult<T>> filter(Predicate<SingularSearchResult<T>> predicate) {
        return predicate.test(this)
                ? Stream.of(this)
                : Stream.empty();
    }

}

