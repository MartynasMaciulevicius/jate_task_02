package lt.vu.mif.jate.tasks.task02.store;

import lombok.Getter;
import lt.vu.mif.jate.tasks.task02.search.SearchManager;
import lt.vu.mif.jate.tasks.task02.store.model.Item;
import org.apache.commons.lang3.text.StrBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.stream.Collector;
import java.util.stream.Stream;

/**
 * Top level domain store class.
 *
 * @author valdo
 */
@Getter
public class Store extends SearchManager<Item> {

    /**
     * List of fields used in store
     */
    public static final String TITLE_FIELD = "TITLE";
    public static final String SUBTITLE_FIELD = "SUBTITLE";
    public static final String DESCRIPTION_FIELD = "DESCRIPTION";
    public static final String CATEGORY_FIELD = "CATEGORY";
    public static final String AUTHOR_FIELD = "AUTHOR";
    public static final String PUBLISHER_FIELD = "PUBLISHER";

    /**
     * Load objects from the input stream.
     *
     * @param is input stream.
     */
    public final void load(final InputStream is) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(
                    new BufferedReader(
                            new InputStreamReader(is, "UTF-8")).lines()
                            .collect(
                                    Collector.of(StrBuilder::new,
                                            StrBuilder::append,
                                            StrBuilder::append)).toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed");
        }

        Stream.iterate(0, i -> i + 1)
                .limit(jsonArray.length())
                .map(jsonArray::getJSONObject)
                .forEach(object -> this.add(object));
    }

    private void add(JSONObject obj) {
        getItems().add(ItemFactory.createUnknown(obj));
    }

}
