package lt.vu.mif.jate.tasks.task02.search.operation.operand;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Literal operand.
 *
 * @author valdo
 */
@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
public class Literal implements Operand {

    /**
     * Literal value.
     */
    private final String value;

}
