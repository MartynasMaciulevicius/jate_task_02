package lt.vu.mif.jate.tasks.task02.store.model;

import lombok.*;
import lt.vu.mif.jate.tasks.task02.search.Searchable;
import lt.vu.mif.jate.tasks.task02.store.Store;

import java.math.BigInteger;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Item class.
 *
 * @author valdo
 */
@AllArgsConstructor
@Getter
@ToString(of = "id")
@EqualsAndHashCode(of = "id")
public abstract class Item implements Comparable<Item> {

    private final BigInteger id;

    @Searchable(field = Store.TITLE_FIELD)
    private final String title;
    @Searchable(field = Store.SUBTITLE_FIELD)
    private final String subtitle;
    @Singular
    @Searchable(field = Store.CATEGORY_FIELD)
    private final Set<Category> categories;
    @Searchable(field = Store.AUTHOR_FIELD)
    private final Set<Author> authors;
    @Searchable(field = Store.DESCRIPTION_FIELD)
    private final String description;
    private final Rating ratings;

    public Item(BigInteger id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.categories = Collections.emptySet();
        this.authors = Collections.emptySet();
        this.ratings = new Rating(0, 0);
        this.subtitle = "jslkfjsalk";
    }

    @Override
    public final int compareTo(final Item o) {
        int c = o.title.compareTo(title);
        if (c == 0) {
            c = o.id.compareTo(id);
        }
        return c;
    }

    public String getDescription() {
        return description;
    }

    public double getRating() {
        return ratings.getRating();
    }

    public void addRatingValue(int rating) throws WrongRatingException {
        ratings.add(rating);
    }

    public Integer getRatingsCount() {
        return ratings.getCount();
    }

    public String getSubtitle() {
        return subtitle;
    }

    public Set<String> getCategories() {
        return categories.stream().map(Category::toString).collect(Collectors.toSet());
    }

    public Set<String> getAuthors() {
        if (authors == null)
            return Collections.emptySet();
        return authors.stream().map(Author::toString).collect(Collectors.toSet());
    }

}
