package lt.vu.mif.jate.tasks.task02.store.model.builderHelpers;

import lombok.Getter;
import lt.vu.mif.jate.tasks.task02.store.model.Category;
import lt.vu.mif.jate.tasks.task02.store.model.Rating;

import java.util.HashSet;
import java.util.Set;

/**
 * Created on 4/17/16.
 *
 * @author invertisment
 */
public class AuthorHelper {

    private final RatingHelper ratingHelper = new RatingHelper();

    @Getter
    private final Set<String> authors = new HashSet<>();
    @Getter
    private final Set<String> subtitles = new HashSet<>();

    public AuthorHelper author(String author) {
        this.authors.add(author);
        return this;
    }

    public AuthorHelper subtitle(String subtitle) {
        subtitles.add(subtitle);
        return this;
    }

    public RatingHelper rating(double rating, int count) {
        return ratingHelper.rating(rating, count);
    }

    public RatingHelper category(String category) {
        return ratingHelper.category(category);
    }

    public Set<Category> getCategories() {
        return ratingHelper.getCategories();
    }

    public Rating getRatings() {
        return ratingHelper.getRatings();
    }
}
