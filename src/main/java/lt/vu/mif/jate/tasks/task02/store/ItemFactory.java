package lt.vu.mif.jate.tasks.task02.store;

import lt.vu.mif.jate.tasks.task02.store.model.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 3/10/16.
 *
 * @author invertisment
 */
public class ItemFactory {

    private static final Collection<String> movieNonCats = Collections.singletonList("Movies");

    public static Book createBook(JSONObject bookJson) throws WrongItemFormatException {

        Integer ratingsCount = (Integer) getNullable(bookJson::get, "ratingsCount", bookJson);
        Double averageRating = getNullable(bookJson::getDouble, "averageRating", bookJson);

        try {
            return Book.builder()
                    .id(new BigInteger(bookJson.getString("isbn")))
                    .title(getNullable(bookJson::getString, "title", bookJson))
                    .subtitle(getNullable(bookJson::getString, "subtitle", bookJson))
                    .pages(getNullable(bookJson::getInt, "pageCount", bookJson))
                    .ratings(new Rating(ratingsCount == null ? 0 : ratingsCount, averageRating == null ? 0 : averageRating))
                    .categories(parseArray(getNullable(bookJson::getJSONArray, "categories", bookJson), Category::new))
                    .authors(parseArray(getNullable(bookJson::getJSONArray, "authors", bookJson), Author::new))
                    .publisher(getNullable(bookJson::getString, "publisher", bookJson))
                    .description(getNullable(bookJson::getString, "description", bookJson))
                    .build();
        } catch (JSONException j) {
            throw new WrongItemFormatException(j);
        }
    }

    private static <Out> Set<Out> parseArray(JSONArray object, Function<String, Out> creator) {
        if (object == null) return new HashSet<>();
        return Stream.iterate(0, i -> i + 1)
                .limit(object.length())
                .map(object::getString)
                .map(creator::apply)
                .collect(Collectors.toSet());
    }

    private static <T> T getNullable(Function<String, T> fun, String input, JSONObject object) {
        return object.has(input) ? fun.apply(input) : null;
    }

    public static Movie createMovie(JSONObject j) throws WrongItemFormatException {

        Integer ratingsCount = (Integer) getNullable(j::get, "numReviews", j);
        Double averageRating = getNullable(j::getDouble, "customerRating", j);

        try {

            return Movie.builder()
                    .id(new BigInteger(String.valueOf(j.getInt("id"))))
                    .title(getNullable(j::getString, "name", j))
                    .ratings(new Rating(ratingsCount == null ? 0 : ratingsCount, averageRating == null ? 0 : averageRating))
                    .categories(parseMovieCat(getNullable(j::getString, "categoryPath", j)))
                    .description(getNullable(j::getString, "longDescription", j))
                    .build();
        } catch (JSONException je) {
            throw new WrongItemFormatException(je);
        }
    }

    private static Set<Category> parseMovieCat(String categories) {
        return Stream.of(categories.split("/"))
                .filter(a -> !movieNonCats.contains(a))
                .map(Category::new)
                .collect(Collectors.toSet());
    }

    public static Item createUnknown(JSONObject object) {
        if (isBook(object)) {
            return createBook(object);
        }
        return createMovie(object);
    }

    private static boolean isBook(JSONObject object) {
        return object.has("isbn");
    }

}
