package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchables;

import lt.vu.mif.jate.tasks.task02.search.Searchable;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.helpers.BinarySearchResult;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.helpers.SearchResultHelper;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults.BaseSearchResult;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults.SingularSearchResult;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 5/11/16.
 *
 * @author invertisment
 */
public class MultipleSearchables<T> {
    final T object;
    final Collection<Field> fields;

    public MultipleSearchables(T object, Stream<Field> fields) {
        this.object = object;
        this.fields = fields.collect(Collectors.toList());
    }

    public Stream<SingularSearchResult<T>> unwrap() {
        return SearchResultHelper.getValues(object, fields.stream())
                .flatMap((Optional<Object> o) -> SingularSearchResult.create(object, o));
    }

//    public Stream<BinarySearchResult<T>> lalala() {
//        if (fields.size() != 2) {
//            throw new IllegalArgumentException("Fields size must be 2");
//        }
//        Iterator<Field> fieldIterator = fields.iterator();
//
//        return BinarySearchResult
//
//    }

//    public Stream<BaseSearchResult>

//    public static <T> Optional<MultipleSearchables<T>> create(T object, Collection<Field> stream) {
//
//    }

    // TODO
//    public Optional<MultipleSearchables> filter(final String attributeType) {
//        Stream<Field> fieldStream = fields.stream().filter(singleField ->
//                isMatching(
//                        getSearchablesForField(singleField),
//                        attributeType));
//    }

    public T getObject() {
        return object;
    }

    public Collection<Field> getFields() {
        return fields;
    }


//    public static <T> Stream<MultipleSearchables<T>> getMatchingSearchables(
//            Stream<MultipleSearchables<T>> toFilter,
//            final String attributeType) {
//        return toFilter
//                .flatMap(ms -> {
//                    Collection<Field> fieldStream = ms.getFields().stream()
//                            .filter(singleField ->
//                                    isMatching(
//                                            getSearchablesForField(singleField),
//                                            attributeType))
//                            .collect(Collectors.toList());
//                    if (fieldStream.size() > 0)
//                        return Stream.of(
//                                new MultipleSearchables<>(ms.getObject(), fieldStream.stream()));
//                    return Stream.empty();
//                });
//    }

    public static <T, M extends BaseSearchResult<T, M>> Stream<M> getMatchingSearchables(
            Stream<MultipleSearchables<T>> toFilter,
            BiFunction<T, Collection<Field>, Stream<M>> mapper
    ) {

        return toFilter
                .flatMap(ms -> {
                    return mapper.apply(ms.object, ms.getFields());

//                    Collection<Field> fieldStream = ms.getFields().stream()
//                            .filter(singleField ->
//                                    isMatching(
//                                            getSearchablesForField(singleField),
//                                            attributeType))
//                            .collect(Collectors.toList());
//                    if (fieldStream.size() > 0)
//                        return Stream.of(
//                                new MultipleSearchables<>(ms.getObject(), fieldStream.stream()));
//                    return Stream.empty();
                });
    }

    /**
     * Will consume fields stream
     */
    public static <T> Stream<BinarySearchResult<T>> produceBinarySearchResult(
            T result,
            Collection<Field> fields,
            Function<Stream<Field>, Optional<Field>> firstFieldExtractor,
            Function<Stream<Field>, Optional<Field>> secondFieldExtractor
    ) {

        Optional<Field> firstField = firstFieldExtractor.apply(fields.stream());
        Optional<Field> secondField = secondFieldExtractor.apply(fields.stream());

        return BinarySearchResult.create(
                result,
                firstField.flatMap(
                        val -> SearchResultHelper.getValue(
                                result,
                                val)),
                secondField.flatMap(
                        val -> SearchResultHelper.getValue(
                                result,
                                val)
                ));
    }

    /**
     * Will consume fields stream
     */
    public static <T> Stream<SingularSearchResult<T>> produceSingularSearchResult(
            T result,
            Collection<Field> fields,
            Function<Stream<Field>, Optional<Field>> firstFieldExtractor
    ) {

        Optional<Field> firstField = firstFieldExtractor.apply(fields.stream());

        return SingularSearchResult.create(
                result,
                firstField.flatMap(
                        val -> SearchResultHelper.getValue(
                                result,
                                val)));
    }

    public static boolean isMatching(Stream<Searchable> searchables, String name) {
        return searchables.filter(searchable -> searchable.field().equals(name))
                .count() > 0;
    }

//    public static <T> BiFunction<T, Collection<Field>, Stream<BinarySearchResult<T>>> extractSearchResult(
//            final String firstAttr,
//            final String secondAttr) {
//
//        return (t, fields1) -> produceBinarySearchResult(
//                t,
//                fields1,
//                fieldStream1 -> fieldStream1.filter(
//                        field -> isMatching(getSearchablesForField(field), firstAttr)).findAny(),
//                fieldStream2 -> fieldStream2.filter(
//                        field -> isMatching(getSearchablesForField(field), secondAttr)).findAny()
//        );
//    }

//    public static <T, M extends BaseSearchResult<T>> BiFunction<T, Collection<Field>, Stream<M>> extractSearchResult(
//            final String attributeType) {
//
//        return (t, fields) -> produceSingularSearchResult(
//                t,
//                fields,
//                (Stream<Field> fieldStream2) -> fieldStream2.filter(
//                        field -> isMatching(getSearchablesForField(field), attributeType)).findAny()
//        );
//    }

    public static Stream<Searchable> getSearchablesForField(Field field) {
        return Arrays.asList(
                field.getAnnotationsByType(
                        Searchable.class))
                .stream();
    }

}

