package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created on 5/10/16.
 *
 * @author invertisment
 */
public interface BaseSearchResult<T, This> {

    T getResult();

    Stream<This> filter(Predicate<This> predicate);

}

