package lt.vu.mif.jate.tasks.task02.search;

import lt.vu.mif.jate.tasks.task02.search.operation.*;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.Field;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.Literal;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.Operand;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.OperandHelper;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.helpers.BinarySearchResult;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.helpers.SearchResultHelper;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults.BaseSearchResult;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults.SingularSearchResult;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchables.MultipleSearchables;
import lt.vu.mif.jate.tasks.task02.search.operation.operator.BinaryOperator;
import lt.vu.mif.jate.tasks.task02.search.operation.operator.SingularOperator;
import lt.vu.mif.jate.tasks.task02.search.struct.MyFavouriteSortedSet;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Search Manager to be extended by domain class.
 *
 * @param <T> collection element type.
 * @author valdo
 */
public abstract class SearchManager<T extends Comparable<T>> {

    private final Set<T> items = new TreeSet<>();
    private final BiConsumer<TreeSet<T>, TreeSet<T>> conjunctionCombiner = AbstractCollection::retainAll;

    /**
     * Get collection of elements to search in.
     *
     * @return collection.
     */
    protected Collection<T> getSearchItems() {
        return items;
    }

    /**
     * Search for items in collection.
     *
     * @param operation operation for the search.
     * @return found elements.
     */
    public SortedSet<T> findItems(final Operation operation) {
        return createSet(
                findItemsInternal(
                        operation
                ).collect(Collectors.toSet()));
    }

    private Stream<T> findItemsInternal(final Operation operation) {
        if (operation instanceof Negation) {
            return handleNegation((Negation) operation);
        }

        if (operation instanceof FinalOperation)
            return handleFinalOp((FinalOperation) operation);

        if (operation instanceof Junction) {
            return handleJunction((Junction) operation);
        }

        throw new RuntimeException("Unimplemented");
    }

    private RuntimeException getUnimplementedThrowable() {
        return new RuntimeException("Unimplemented");
    }

    private Stream<T> handleNegation(Negation operation) {
        Stream<T> out = findItemsInternal(operation.operation);
        Set<T> collect = getSearchItems().stream().collect(Collectors.toSet());
        collect.removeAll(out.collect(Collectors.toList()));
        return collect.stream();
    }

    private Stream<T> handleBinaryOpFieldLiteral(BinaryOperator operator, Operand first, Operand second) {

        if (first instanceof Literal) {
            if (second instanceof Literal)
                return handleBinaryOpLiteralLiteral(operator, (Literal) first, (Literal) second);
            else if (second instanceof Field)
                return handleBinaryOpFieldLiteral(operator, second, first);
        } else if (first instanceof Field) {
            if (second instanceof Literal)
                return handleBinaryOpFieldField(
                        operator,
                        OperandHelper.KeyValue.create(first, second));
            else if (second instanceof Field)
                return handleBinaryOpFieldLiteral(operator, (Field) first, (Field) second);
        }

        throw getUnimplementedThrowable();
    }

    private Stream<T> handleBinaryOpLiteralLiteral(BinaryOperator operator, Literal first, Literal second) {
        return handleBinaryOpInternal(operator, first, second)
                ? getSearchItems().stream()
                : Stream.empty();
    }

    private boolean handleBinaryOpInternal(BinaryOperator operator, Literal first, Literal second) {
        switch (operator) {
            case EQUALS:
                return OperandHelper.equals(first, second);
            case CONTAINS:
                return OperandHelper.contains(first, second);
            case MATCHES:
                return OperandHelper.matches(first, second);
        }

        throw getUnimplementedThrowable();
    }

    private Stream<T> handleBinaryOpFieldField(BinaryOperator operator, Optional<OperandHelper.KeyValue> maybeKeyValue) {
        if (maybeKeyValue.isPresent()) {
            OperandHelper.KeyValue keyValue = maybeKeyValue.get();
            return getResults(
                    handleBinaryFieldAndLiteral(
                            getMatchingAttributes(
                                    keyValue.key),
                            operator,
                            keyValue.value));
        }

        throw new RuntimeException("Unimplemented");
    }

    private Stream<SingularSearchResult<T>> handleBinaryFieldAndLiteral(
            Stream<SingularSearchResult<T>> stream,
            BinaryOperator operator,
            Optional<String> value) {
        if (!value.isPresent()) {
            throw new RuntimeException("Unimplemented");
        }

        return stream.flatMap(m -> m.filter(
                getBinaryFieldAndLiteralPredicate(
                        operator,
                        value)));
    }

    private Stream<T> handleBinaryOpFieldLiteral(BinaryOperator operator, Field first, Field second) {
        Optional<String> firstValue = OperandHelper.getValue(first);
        Optional<String> secondValue = OperandHelper.getValue(second);

        if (OperandHelper.anyNull(firstValue, secondValue))
            return Stream.empty();

        return handleBinaryOpInternal(operator, firstValue.get(), secondValue.get());
    }

    private Stream<T> handleBinaryOpInternal(BinaryOperator operator, String first, String second) {
        switch (operator) {
            case EQUALS:
                return handleBinaryOpInternalEquals(first, second);
            case CONTAINS:
                return handleBinaryOpInternalContains(first, second);
            case MATCHES:
                break;
        }
        throw getUnimplementedThrowable();
    }

    private Stream<T> handleBinaryOpInternalEquals(String first, String second) {
        return getResults(
                findByAttribute(
                        getMatchingAttributes(first, second),
                        binaryResult -> Objects.equals(
                                binaryResult.getFirstField(),
                                binaryResult.getSecondField())));

    }

    private Stream<T> handleBinaryOpInternalContains(String first, String second) {

        return getResults(
                findByAttribute(
                        getMatchingAttributes(first, second),
                        tBinarySearchResult ->
                                OperandHelper.contains(tBinarySearchResult.getFirstField(),
                                        tBinarySearchResult.getSecondField().map(Object::toString))));
    }

    private Predicate<SingularSearchResult<T>> getBinaryFieldAndLiteralPredicate(
            BinaryOperator operator,
            Optional<String> value) {
        switch (operator) {
            case EQUALS:
                return o -> OperandHelper.equals(o.getFieldValue(), value);
            case CONTAINS:
                return o -> OperandHelper.contains(o.getFieldValue(), value);
            case MATCHES:
                return o -> OperandHelper.matches(o.getFieldValue(), value);
        }
        throw new RuntimeException("Unimplemented");
    }

    private Stream<T> handleJunction(Junction junction) {

        if (junction instanceof Conjunction)
            return handleConjunction(junction);

        if (junction instanceof Disjunction)
            return handleDisjunction(junction);

        throw new RuntimeException("Unimplemented");
    }

    private Stream<T> handleDisjunction(Junction disjunction) {
        return unwrapJunction(disjunction)
                .reduce(Stream.empty(), Stream::concat);
    }

    private Stream<T> handleConjunction(Junction conjunction) {
        return unwrapJunction(conjunction)
                .map(s -> s.collect(Collectors.toSet()))
                .collect(TreeSet::new,

                        new BiConsumer<TreeSet<T>, Set<T>>() {
                            boolean hasAddedOnce = false;

                            @Override
                            public void accept(TreeSet<T> ts, Set<T> t) {
                                if (hasAddedOnce) {
                                    ts.retainAll(t);
                                } else {
                                    hasAddedOnce = true;
                                    ts.addAll(t);
                                }
                            }
                        },

                        conjunctionCombiner)
                .stream();
    }

    private Stream<Stream<T>> unwrapJunction(Junction junction) {
        return junction
                .getOperations()
                .stream()
                .map(this::findItemsInternal);
    }

    private Stream<T> handleSingularOp(SingularOperation singularOperation) {
        return resolveSingularOperationOutput(singularOperation.getOperand(), singularOperation.getOperator());
    }

    private Stream<T> handleFinalOp(FinalOperation operation) {

        if (operation instanceof SingularOperation)
            return handleSingularOp((SingularOperation) operation);

        if (operation instanceof BinaryOperation) {
            BinaryOperation binaryOperation = (BinaryOperation) operation;
            return handleBinaryOpFieldLiteral(binaryOperation.getOperator(), binaryOperation.getOperand1(), binaryOperation.getOperand2());
        }

        throw new RuntimeException("Unimplemented");
    }

    private Stream<T> resolveSingularOperationOutput(Operand operand, SingularOperator operator) {

        if (operand instanceof Literal) {
            return handleLiteralOp(
                    ((Literal) operand).getValue(),
                    operator);
        }

        if (operand instanceof Field) {
            return handleFieldOp(
                    ((Field) operand).getName(),
                    operator);
        }

        throw new RuntimeException("Unimplemented");
    }

    private Stream<T> handleFieldOp(String fieldName, SingularOperator operator) {
        switch (operator) {
            case EMPTY:
                return getResults(
                        findByAttribute(
                                getMatchingAttributes(fieldName), result -> result
                                        .getFieldValue()
                                        .filter(o -> Objects.equals(
                                                o,
                                                ""
                                        )).isPresent()));

            case NULL:
                return getResults(
                        findByAttribute(
                                getMatchingAttributes(fieldName),
                                o -> !o.getFieldValue().isPresent()));
        }
        throw new RuntimeException("Unimplemented");
    }

    private Stream<T> handleLiteralOp(String value, SingularOperator operator) {
        boolean match;
        switch (operator) {
            case NULL:
                match = value != null;
                break;
            case EMPTY:
                match = Stream.of("Value", null)
                        .filter(s -> Objects.equals(value, s))
                        .count() > 0;
                break;
            default:
                throw new RuntimeException("Unimplemented");
        }

        if (match)
            return Stream.empty();
        else
            return getSearchItems().stream();

    }

    private SortedSet<T> createSet(Set<T> set) {
        return Collections.unmodifiableSortedSet(
                new MyFavouriteSortedSet<>(set)
        );
    }

    /**
     * Return the list of unique values indicated by the keywords.
     *
     * @param attributeType field to search.
     * @return set of values.
     */
    public Set<String> getValueList(final String attributeType) {
        List<String> collect =
                SearchResultHelper.getValues(
                        getMatchingAttributes(attributeType))
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .map(Object::toString)
                        .distinct()
                        .collect(Collectors.<String>toList());

        return collect.stream().collect(Collectors.toSet());
    }

    private Stream<SingularSearchResult<T>> getMatchingAttributes(final String attributeType) {

        return MultipleSearchables.getMatchingSearchables(
                getSearchables(),
                (t, fields) -> MultipleSearchables.produceSingularSearchResult(
                        t,
                        fields,
                        fieldStream2 -> fieldStream2.filter(
                                field -> MultipleSearchables.isMatching(
                                        MultipleSearchables.getSearchablesForField(field), attributeType)).findAny()
                ));
    }

    private Stream<BinarySearchResult<T>> getMatchingAttributes(final String firstAttributeType, final String secondAttributeType) {

        return MultipleSearchables.getMatchingSearchables(
                getSearchables(),
                (t, fields) -> MultipleSearchables.produceBinarySearchResult(
                        t,
                        fields,
                        fieldStream2 -> fieldStream2.filter(
                                field -> MultipleSearchables.isMatching(
                                        MultipleSearchables.getSearchablesForField(field), firstAttributeType)).findAny(),
                        fieldStream3 -> fieldStream3.filter(
                                field -> MultipleSearchables.isMatching(
                                        MultipleSearchables.getSearchablesForField(field), secondAttributeType)).findAny()
                ));
    }

    private <U, M extends BaseSearchResult<U, M>> Stream<M> findByAttribute
            (Stream<M> resultStream, Predicate<M> predicate) {
        return resultStream
                .flatMap(searchResult -> searchResult.filter(predicate));
    }

    private <M extends BaseSearchResult<T, M>> Stream<T> getResults(Stream<M> resultStream) {
        return resultStream
                .map(BaseSearchResult::getResult);
    }

    private Stream<MultipleSearchables<T>> getSearchables() {
        return getSearchItems().stream()
                .map(o -> new MultipleSearchables<T>(o, FieldUtils.getAllFieldsList(o.getClass()).stream()
                        .filter(f -> f.isAnnotationPresent(Searchable.class))));
    }

    public Collection<T> getItems() {
        return getSearchItems();
    }

}
