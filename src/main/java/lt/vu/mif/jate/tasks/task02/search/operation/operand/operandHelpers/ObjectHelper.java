package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers;

import lombok.AllArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created on 5/10/16.
 *
 * @author invertisment
 */
public class ObjectHelper {

    public static Stream<Object> decolectionize(Object object) {
        if (object instanceof Collection) {
            @SuppressWarnings("unchecked")
            Collection<Object> values = (Collection<Object>) object;
            return values
                    .stream()
                    .flatMap(ObjectHelper::decolectionize);
        }
        return Stream.of(object);
    }

    public static <T> Stream<Tuple<T>> generateDecartes(Stream<T> firsts, Stream<T> seconds) {
        List<T> secondsList = seconds.collect(Collectors.toList());

        return firsts.flatMap(
                first -> secondsList.stream().map(
                        second -> new Tuple<>(first, second)));
    }

    public static Optional<Stream<Object>> convertMaybeCollection(Optional<Object> maybeCollection) {
        return maybeCollection
                .map(ObjectHelper::decolectionize);
    }

    public static Stream<Optional<Object>> unwrapParentOptional(Optional<Stream<Object>> parentOptionalOfStreams) {
        return parentOptionalOfStreams
                .map(objectStream1 -> objectStream1.map(Optional::ofNullable))
                .orElse(Stream.of(Optional.empty()));
    }

    public static Stream<Optional<Object>> expand(Optional<Object> data) {
        return unwrapParentOptional(
                convertMaybeCollection(data));
    }

    @AllArgsConstructor
    public static class Tuple<T> {
        private final T first;
        private final T second;

        public Optional<T> getFirst() {
            return Optional.ofNullable(first);
        }

        public Optional<T> getSecond() {
            return Optional.ofNullable(second);
        }
    }
}
