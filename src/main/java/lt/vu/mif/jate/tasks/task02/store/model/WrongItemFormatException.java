package lt.vu.mif.jate.tasks.task02.store.model;

/**
 * Created by martin on 3/9/16.
 */
public class WrongItemFormatException extends RuntimeException {
    public WrongItemFormatException(Throwable cause) {
        super(cause);
    }
}
