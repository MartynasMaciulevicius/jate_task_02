package lt.vu.mif.jate.tasks.task02.store.model.builderHelpers;

import lt.vu.mif.jate.tasks.task02.store.model.Category;
import lt.vu.mif.jate.tasks.task02.store.model.Rating;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created on 4/17/16.
 *
 * @author invertisment
 */
public class RatingHelper {
    private final Rating ratings = new Rating(0, 0);
    private final Set<Category> categories = new HashSet<>();

    public RatingHelper rating(double rating, int count) {
        ratings.add(Stream.iterate((int) rating, number -> number).limit(count));
        return this;
    }

    public RatingHelper category(String category) {
        categories.add(new Category(category));
        return this;
    }

    public Rating getRatings() {
        return ratings;
    }

    public Set<Category> getCategories() {
        return categories;
    }
}
