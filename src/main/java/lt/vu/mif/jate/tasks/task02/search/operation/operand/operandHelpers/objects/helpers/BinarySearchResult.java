package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.helpers;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults.BaseSearchResult;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.ObjectHelper.expand;

/**
 * Created on 5/10/16.
 *
 * @author invertisment
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BinarySearchResult<T> implements BaseSearchResult<T, BinarySearchResult<T>> {

    private final T result;
    private final Optional<Object> firstField;
    private final Optional<Object> secondField;

    public static <U> Stream<BinarySearchResult<U>> create(U result, Optional<Object> firstField, Optional<Object> secondField) {
        return expand(firstField)
                .flatMap(objectOptional -> expand(secondField)
                        .map(objectOptional1 -> new BinarySearchResult<>(
                                result,
                                objectOptional,
                                objectOptional1)));
    }

    @Override
    public Stream<BinarySearchResult<T>> filter(Predicate<BinarySearchResult<T>> predicate) {
        return predicate.test(this)
                ? Stream.of(this)
                : Stream.empty();
    }
}

