package lt.vu.mif.jate.tasks.task02.search.operation;

import lombok.AccessLevel;
import lombok.Getter;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Junction operation.
 *
 * @author valdo
 */
@Getter(AccessLevel.PROTECTED)
public abstract class Junction implements Operation {

    /**
     * A list of operations to connect with the junction.
     */
    private final List<Operation> operations = new LinkedList<>();

    /**
     * Add operation to the junction.
     *
     * @param op operation.
     * @return this class.
     */
    public final Junction add(final Operation op) {
        operations.add(op);
        return this;
    }

    public Collection<Operation> getOperations() {
        return Collections.unmodifiableList(operations);
    }

}
