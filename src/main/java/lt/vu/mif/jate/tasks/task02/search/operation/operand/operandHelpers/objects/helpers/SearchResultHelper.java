package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.helpers;

import lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers.objects.searchResults.SingularSearchResult;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.lang.reflect.Field;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created on 5/11/16.
 *
 * @author invertisment
 */
public class SearchResultHelper {

    public static <T> Stream<Optional<Object>> getValues(Stream<SingularSearchResult<T>> resultStream) {
        return resultStream
                .map(SingularSearchResult::getFieldValue);
    }

    public static Stream<Optional<Object>> getValues(Object object, Stream<Field> fieldStream) {
        return fieldStream.map((Field field) -> getValue(object, field));
    }

    public static Optional<Object> getValue(Object object, Field field) {
        try {
            return Optional.ofNullable(FieldUtils.readField(field, object, true));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}
