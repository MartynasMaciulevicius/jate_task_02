package lt.vu.mif.jate.tasks.task02.store.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Item class.
 *
 * @author mm
 */
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Rating implements Comparable<Rating> {
    private int count;
    private double averageRating;

    public Rating(Collection<Integer> ratings) {
        this(ratings.size(), ratings.stream().mapToInt(i -> i).sum());
    }

    @Override
    public int compareTo(Rating o) {
        return Double.compare(getRating(), o.getRating());
    }

    public void add(int rating) throws WrongRatingException {
        if (rating > 5) throw new WrongRatingException(rating);
        averageRating = (averageRating * count + rating) / (count + 1);
        count++;
    }

    public void add(Stream<Integer> ratings) throws WrongRatingException {
        ratings.forEach(this::add);
    }

    public double getRating() {
        return averageRating;
    }
}
