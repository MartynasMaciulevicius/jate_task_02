package lt.vu.mif.jate.tasks.task02.search.operation.operand.operandHelpers;

import lt.vu.mif.jate.tasks.task02.search.operation.operand.Field;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.Literal;
import lt.vu.mif.jate.tasks.task02.search.operation.operand.Operand;

import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;

/**
 * Created on 5/9/16.
 *
 * @author invertisment
 */
public class OperandHelper {
    public static boolean equals(Operand first, Operand second) {
        return new Validatinator().validatinate(
                first,
                second,
                (string, string2) -> Objects.equals(first, second));
    }

    public static boolean matches(Operand first, Operand second) {
        return new Validatinator().validatinate(
                first,
                second,
                String::matches);
    }

    public static boolean contains(Operand first, Operand second) {
        return new Validatinator().validatinate(
                first,
                second,
                String::contains);
    }

    public static boolean equals(Optional<Object> first, Optional<String> second) {
        if (Objects.equals(first, second))
            return true;
        return !(!first.isPresent() || !second.isPresent()) && Objects.equals(first.get().toString(), second.orElseGet(null));
    }

    public static boolean matches(Optional<Object> first, Optional<String> second) {
        if (!second.isPresent())
            return false;
        return first.map(str -> str.toString().matches(second.get())).orElse(false);
    }

    public static boolean contains(Optional<Object> first, Optional<String> second) {
        if (!second.isPresent())
            return false;
        return first.map(str -> str.toString().contains(second.get())).orElse(false);
    }

    public static boolean isSameType(Operand first, Operand second) {
        return first.getClass().equals(second.getClass());
    }

    public static boolean anyNull(Optional first, Optional second) {
        return !first.isPresent() || !second.isPresent();
    }

    public static class KeyValue {
        public final String key;
        public final Optional<String> value;

        public KeyValue(String key, Optional<String> value) {
            this.key = key;
            this.value = value;
        }

        public static Optional<KeyValue> create(Operand first, Operand second) {
            Field key = null;
            if (first instanceof Field)
                key = (Field) first;
            else if (second instanceof Field)
                key = (Field) second;
            if (key == null)
                return Optional.empty();

            Literal value = null;
            if (second instanceof Literal)
                value = (Literal) second;
            else if (first instanceof Literal)
                value = (Literal) first;
            if (value == null)
                return Optional.empty();
            final Literal finalValue = value;

            return getValue(key)
                    .map(s -> new KeyValue(s, getValue(finalValue)));
        }

    }

    public static Optional<String> getValue(Operand operand) {
        if (operand instanceof Literal)
            return Optional.ofNullable(((Literal) operand).getValue());

        if (operand instanceof Field)
            return Optional.ofNullable(((Field) operand).getName());

        return Optional.empty();
    }

    public static boolean isValid(Operand first, Operand second) {
        return !anyNull(getValue(first), getValue(second)) && isSameType(first, second);
    }

    private static class Validatinator {

        boolean validatinate(Operand first, Operand second, BiFunction<String, String, Boolean> matcher) {
            if (!isValid(first, second))
                return false;
            return matcher.apply(
                    getValue(first).get(),
                    getValue(second).get());
        }
    }
}
