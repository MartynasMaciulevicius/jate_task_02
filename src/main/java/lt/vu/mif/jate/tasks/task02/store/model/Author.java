package lt.vu.mif.jate.tasks.task02.store.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

/**
 * Created by martin on 3/10/16.
 */
@AllArgsConstructor
@EqualsAndHashCode
public class Author {
    public final String name;

    @Override
    public String toString() {
        return name;
    }
}
